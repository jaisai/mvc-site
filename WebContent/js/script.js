// Document ready
$(function () {
   
    validateLoginForm();
    dateTimePicker();
    alternateDateTimePicker();
    astraSection();
    validateCreateMeeting();
    validateCreateWorkshop();
    validateRequestToPresent();
    validateMeetingRequest();
    validateWouldLikeToPresent();
    validateWorkshopRequestDetail();
    validateWorkshopRequest();
});

// Login page validation
function validateLoginForm() {
    $("#Username").focusout(function () {
        debugger;
        validateTextBox("Username", "usernameFormGroup");
    })
    $("#Password").focusout(function () {
        debugger;
        validateTextBox("Password", "passwordFormGroup");
    })
    $("#LoginButton").click(function () {
        valid = validateTextBox("Password", "passwordFormGroup");
        return validateTextBox("Username", "usernameFormGroup") && valid;
    });
}
// Function to do date and time picker stuff
function dateTimePicker() {
    if ($("#startdatepicker").length) {
        // Date pickers
        $("#startdatepicker").datetimepicker({
            format: "MM-DD-YYYY"
        });
        $("#enddatepicker").datetimepicker({
            format: "MM-DD-YYYY",
            useCurrent: false
        });
        $("#validatebuildingsDL").hide();
        $("#dateTimeValidation").hide();
        $("#validateRoomTextBox").hide();
        $("#validatePresenter").hide();
        $("#validatePhoneNumber").hide();
        $("#prefStartDateValidation").hide();
        $("#prefStartTimeValidation").hide();
        $("#prefEndDateValidation").hide();
        $("#prefEndTimeValidation").hide();
        // Restrict dates based on start date selected
        $("#startdatepicker").on("dp.change", function (e) {
            $("#enddatepicker").data("DateTimePicker").minDate(e.date);
        });
        $("#enddatepicker").on("dp.change", function (e) {
            $("#startdatepicker").data("DateTimePicker").maxDate(e.date);
        });
        // Time pickers
        $("#starttimepicker").datetimepicker({
            format: "LT",
            stepping: 15
        });
        $("#endtimepicker").datetimepicker({
            format: "LT",
            stepping: 15,
        });
    }
}

function alternateDateTimePicker() {
    if ($("#alternatestartdatepicker").length) {
        // Date pickers
        $("#alternatestartdatepicker").datetimepicker({
            format: "MM-DD-YYYY"
        });
        $("#alternateenddatepicker").datetimepicker({
            format: "MM-DD-YYYY",
            useCurrent: false
        });
        $("#validatebuildingsDL").hide();
        $("#dateTimeValidation").hide();
        $("#validateRoomTextBox").hide();
        $("#alternateDateTimeValidation").hide();
        $("#validatePresenter").hide();
        $("#validatePhoneNumber").hide();
        $("#prefStartDateValidation").hide();
        $("#prefStartTimeValidation").hide();
        $("#prefEndDateValidation").hide();
        $("#prefEndTimeValidation").hide();

        // Restrict dates based on start date selected
        $("#alternatestartdatepicker").on("dp.change", function (e) {
            $("#alternateenddatepicker").data("DateTimePicker").minDate(e.date);
        });
        $("#alternateenddatepicker").on("dp.change", function (e) {
            $("#alternatestartdatepicker").data("DateTimePicker").maxDate(e.date);
        });
        // Time pickers
        $("#alternatestarttimepicker").datetimepicker({
            format: "LT",
            stepping: 15
        });
        $("#alternateendtimepicker").datetimepicker({
            format: "LT",
            stepping: 15,
        });
    }
}

function astraSection() {
    if ($("#requestSent").length) {
        // Date pickers
        $("#requestSent").datetimepicker({
            format: "MM-DD-YYYY"
        });
        $("#roomConfirmation").datetimepicker({
            format: "MM-DD-YYYY",
            useCurrent: false
        });
   
        // Restrict dates based on start date selected
        $("#requestSent").on("dp.change", function (e) {
            $("#roomConfirmation").data("DateTimePicker").minDate(e.date);
        });
        $("#roomConfirmation").on("dp.change", function (e) {
            $("#requestSent").data("DateTimePicker").maxDate(e.date);
        });
     
    }
}

// Create/edit meeting form validation
function validateCreateMeeting() {
    $("#createMeeting").click(function (e) {
        var isValid = true;
        if ($("#buildingsDropdown").val() == 0) {
            $("#validatebuildingsDL").show();
            isValid = false;
        } else {
            $("#validatebuildingsDL").hide();
        }
        if ($("#roomTextBox").val() == 0 || $("#roomTextBox").val().length > 40 ) {
            $("#validateRoomTextBox").show();
            isValid = false;
        } else {
            $("#validateRoomTextBox").hide();
        }
        if ($("#startDate").val().toString().trim() == "") {
            $("#prefStartDateValidation").show();
            isValid = false;
        } else {
            $("#prefStartDateValidation").hide();
        }
        if ($("#startTime").val().toString().trim() == "") {
            $("#prefStartTimeValidation").show();
            isValid = false;
        } else {
            $("#prefStartTimeValidation").hide();
        }
        if ($("#endDate").val().toString().trim() == "") {
            $("#prefEndDateValidation").show();
            isValid = false;
        } else {
            $("#prefEndDateValidation").hide();
        }
        if ($("#endTime").val().toString().trim() == "") {
            $("#prefEndTimeValidation").show();
            isValid = false;
        } else {
            $("#prefEndTimeValidation").hide();
        }
        return isValid

    });
}

// Create/edit workshop from validation
function validateCreateWorkshop() {
    $("#WSName").focusout(function () {
        validateTextBox("WSName", "nameFormGroup");
    });
    $("#WSDescription").focusout(function () {
        validateTextBox("WSDescription", "descriptionFormGroup");
    });
    $("#createWorkshop").click(function () {
        validForm = true;
        if (!$("#WSName").val()) {
            $("#nameFormGroup").addClass("has-error");
            validForm = false;
        }
        else {
            $("#nameFormGroup").removeClass("has-error");
        }
        if (!$("#WSDescription").val()) {
            $("#descriptionFormGroup").addClass("has-error");
            validForm = false;
        }
        else {
            $("#descriptionFormGroup").removeClass("has-error");
        }
        return validForm;
    });
}
// Shows error if textbox is empty, return true if not empty
function validateTextBox(textBoxID, formGroupID) {
    isValid = true;
    if (!$("#" + textBoxID).val()) {
        $("#" + formGroupID).addClass("has-error");
        isValid = false;
    }
    else {
        $("#" + formGroupID).removeClass("has-error");
        $("#" + formGroupID).addClass("has-success");
    }
    return isValid;
}

function validateWouldLikeToPresent() {
   
    $("#presenterTextBox").focusout(function () {
        validateTextBox("presenterTextBox", "presenterTextBoxDiv");
    });
    $("#phoneNumberTextBox").focusout(function () {
        validateTextBox("phoneNumberTextBox", "phoneNumberDiv");
    });
    $("#submitForm").click(function () {
        validForm = true;
        alert(document.getElementById('presentCheckbox').checked);
        if (document.getElementById('presentCheckbox').checked) {
            alert(document.getElementById('presentCheckbox').checked);
            if (!$("#presentertextbox").val()) {
                $("#presentertextboxdiv").addclass("has-error"); //for the presenter text box
                validform = false;
            }
            else {
                $("#presentertextboxdiv").removeclass("has-error");
            }
            if (!$("#phonenumbertextbox").val()) {
                $("#phonenumberdiv").addclass("has-error"); // for the phone number text box
                validform = false;
            }
            else {
                $("#phonenumberdiv").removeclass("has-error");
            }
        }
        return validForm;
    });
}

function validateRequestToPresent(textBoxID, formGroupID) {
   
    $("#requesterTextBox").focusout(function () {
        validateTextBox("requesterTextBox", "topicBox");
    });
    $("#requestPresentDescArea").focusout(function () {
        validateTextBox("descriptionTextArea", "requestPresentDescArea");
    });  
 
    $("#submitForm").click(function () {
        validForm = true;
        if (!$("#requesterTextBox").val()) {
            $("#topicBox").addClass("has-error");
            validForm = false;
        }
        else {
            $("#topicBox").removeClass("has-error");
        }
        if (!$("#descriptionTextArea").val()) {
            $("#requestPresentDescArea").addClass("has-error");
            validForm = false;
        }
        else {
            $("#requestPresentDescArea").removeClass("has-error");
        }
     
        return validForm;
    });
}

function validateWorkshopRequest() {
    $("#topicTextBox").focusout(function () {
        validateTextBox("topicTextBox", "topicBox");
    });
    $("#requestPresentDescArea").focusout(function () {
        validateTextBox("descriptionTextArea", "requestPresentDescArea");
    });
     $("#submitterTextBox").focusout(function () {
        validateTextBox("submitterTextBox", "presenterTextBoxDiv");
    });
    $("#phoneNumberTextBox").focusout(function () {
        validateTextBox("phoneNumberTextBox", "phoneNumberDiv");
    });
    $("#saveForm").click(function () {
        validForm = true;
        if (!$("#submitterTextBox").val()) {
            $("#presentertextboxdiv").addclass("has-error"); //for the presenter text box
            validform = false;
        }
        else {
            $("#presentertextboxdiv").removeclass("has-error");
        }
        if (!$("#phonenumbertextbox").val()) {
            $("#phonenumberdiv").addclass("has-error"); // for the phone number text box
            validform = false;
        }
        else {
            $("#phonenumberdiv").removeclass("has-error");
        }
        if (!$("#topicTextBox").val()) {
            $("#topicBox").addClass("has-error");
            validForm = false;
        }
        else {
            $("#topicBox").removeClass("has-error");
        }
        if (!$("#descriptionTextArea").val()) {
            $("#requestPresentDescArea").addClass("has-error");
            validForm = false;
        }
        else {
            $("#requestPresentDescArea").removeClass("has-error");
        }
        return validForm;
    });
}

function validateMeetingRequest() {
   
    $("#submitForm").click(function (e) {
        var isValid = true;
        var isDisabled = $('#roomTextBox').prop('disabled');
        if ($("#buildingsDropdown").val() == 0) {
            $("#validatebuildingsDL").show();
            isValid = false;
        } else {
            $("#validatebuildingsDL").hide();
        }
        if (isDisabled) {
            $("#validateRoomTextBox").hide();
        }
        else if ($("#roomTextBox").val() == 0 || $("#roomTextBox").val().length > 40) {
            $("#validateRoomTextBox").show();
        }
        else {
            $("#validateRoomTextBox").hide();
        }
        if ($("#startDate").val().toString().trim() == "") {
            $("#prefStartDateValidation").show();
            isValid = false;
        } else {
            $("#prefStartDateValidation").hide();
        }
        if ($("#startTime").val().toString().trim() == "") {
            $("#prefStartTimeValidation").show();
            isValid = false;
        } else {
            $("#prefStartTimeValidation").hide();
        }
        if ($("#endDate").val().toString().trim() == "") {
            $("#prefEndDateValidation").show();
            isValid = false;
        } else {
            $("#prefEndDateValidation").hide();
        }
        if ($("#endTime").val().toString().trim() == "") {
            $("#prefEndTimeValidation").show();
            isValid = false;
        } else {
            $("#prefEndTimeValidation").hide();
        }
        if ($("#presenterTextBox").val().toString().trim() == "") {
            $("#validatePresenter").show();
            isValid = false;
        } else {
            $("#validatePresenter").hide();
        }
        if ($("#phoneNumberTextBox").val() == 0 || $("#phoneNumberTextBox").val().length > 12) {
            $("#validatePhoneNumber").show();
            isValid = false;
        } else {
            $("#validatePhoneNumber").hide();
        }
        return isValid;

    });
}

function validateWorkshopRequestDetail() {

    $("#saveForm").click(function (e) {
        var isValid = true;
        var isDisabled = $('#roomTextBox').prop('disabled');
        if ($("#buildingsDropdown").val() == 0) {
            $("#validatebuildingsDL").show();
            isValid = false;
        } else {
            $("#validatebuildingsDL").hide();
        }
        if (isDisabled) {
            $("#validateRoomTextBox").hide();
        }
        else if ($("#roomTextBox").val() == 0 || $("#roomTextBox").val().length > 40) {
            $("#validateRoomTextBox").show();
        }
        else {
            $("#validateRoomTextBox").hide();
        }
        if ($("#startDate").val().toString().trim() == "") {
            $("#prefStartDateValidation").show();
            isValid = false;
        } else {
            $("#prefStartDateValidation").hide();
        }
        if ($("#startTime").val().toString().trim() == "") {
            $("#prefStartTimeValidation").show();
            isValid = false;
        } else {
            $("#prefStartTimeValidation").hide();
        }
        if ($("#endDate").val().toString().trim() == "") {
            $("#prefEndDateValidation").show();
            isValid = false;
        } else {
            $("#prefEndDateValidation").hide();
        }
        if ($("#endTime").val().toString().trim() == "") {
            $("#prefEndTimeValidation").show();
            isValid = false;
        } else {
            $("#prefEndTimeValidation").hide();
        }
        if ($("#submitterTextBox").val().toString().trim() == "") {
            $("#validatePresenter").show();
            isValid = false;
        } else {
            $("#validatePresenter").hide();
        }
        if ($("#phoneNumberTextBox").val() == 0 || $("#phoneNumberTextBox").val().length > 12) {
            $("#validatePhoneNumber").show();
            isValid = false;
        } else {
            $("#validatePhoneNumber").hide();
        }
        return isValid;

    });
}