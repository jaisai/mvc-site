<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
 <head>
 <link rel="shortcut icon" href="favicon.ico" />
<link rel="stylesheet" href="css/Login.css" type="text/css" />
<link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
<link rel="stylesheet" href="css/style.css" type="text/css" />
 <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
 
 <title>Login</title>


 </head>
 <body>
<div class="aspNetHidden">

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="F288FB3F" />
	<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="Bwch4Pr5HjOMF+cxMuO7zICwVZ+dW1avrWtPrfWv8jExw0rLtrlRJ/cW70sKWlrNmT8XmRBFrJ7+4OoUXTDmF49561rvuso2rBHxFKIRNLTTBSSstMtI/qpgKEnZuguqY8mtyCRGu/cyUvenS2DwbJKFlFb/eefC6BVuckdSJN+hr3nlBLXfdpXQM0eIMTSWl8QRtJ/qmWJ3N2MIX0GcdU1n4LC4E07Q5t8SeT6sABLMluWnvFXgqxpcrFIPu6xNG4b7oJGgGPxA3ZtpQO3KvN5Y/arrJBA8CfR/Ze8P+b5nlwHiH/fp4OM2ZyEQ4HWEM+unD4Q6xniBEnfUBm4COhA4tIxrb48bslpWryerw7ZAReU0l9NHDoyibnmgWgMPjaw1/oSucFJB1EsjGaujIwpsG8NXjVz3+EsDzHRn1ekiiO34BdabuEQOoGmsEkJTmUoW+zyjIdWF1suLKqJf9ymBcdpgWbXJC8crv55HE34gcRMf5st5fkCkF5TP6gBkyjjCtCrjY9geyVtaOdV+/9c8kpUF8tUSJWgb9aieDqlU1CQzhlVmOXz4wXL/8joMQ5y9Xr0EMzsYWnQ9ouz44YiyYlqdxOb0cPbMXRQUfezFl4IdVwlnAlSSLRj2RgF8kAOTcDNgmydWy8pOFnXEZaMv9RClXvJcxB1tlZA2yWCySA17+GbTZGUdfhLGytEElgA8jRY333YWAJR+0Ze30i5dSQa58oVVjHN/2qYAfoaE0+j3kMeVVWa3Djhp5k1Sv99Popr1VfNdblS/CJ02e5vcLqxpOwQ8eTlW7OWR7CnT3q4VLR2RnB9QFkWPoXz6qWHoDxbzKVVEFSZXOWj3LHH+2jUb3uqAGxex5Xnlur1uTaC00Sq43n8ovgZ+JWKl9xDT2FtXn99rGgTIB0fLW5CzWmqaZStA7xHo5QJUWCnqTDkA0sgqI8iZNA8RxKtrxntffQdL47ZrURZKwLmmj9Q8L0XpIQgNaWY1FliIFBjYP/6/Zq/CYqckSTR77N+ehwJAVTfB13WQKoFZROR7isnnaN1ZcdOXd0zDp+G/WjGoja5EvriEf1tEIHenHR8mwQiA8vUHvn5eyPjRaQNn+pWdcWYDW+2LpjrHzTvX51xmmzMIuMJ4sUyNEHvHrFTS" />
</div>
        <div id="navigationBar" class="workshops">
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <a class="navbar-brand" href="Home.aspx"><img alt="Home" src="img/logo.png" style="width:230px; height:30px;"></a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                        <ul id="navTabs" class="nav navbar-nav">
                             <li id="navHomeTab"><a href="Home.jsp">Home</a></li>
                            <li id="navWorkshopsTab"><a href="UserWorkshops.aspx">Workshops</a></li>
                            <li id="navBadgesTab"><a href="UserBadges.aspx">Attended</a></li>
                            <li id="navAdminTab"><a href="Admin.jsp">Admin</a></li>
                        </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <input type="submit" name="ctl00$logoutButton" value="Logout" id="logoutButton" class="btn btn-primary" title="Logout" />
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
            </div>
        <div class="container" id="MainContainer">
            <div class="row">
                <div id="LeftContentContainer" class="col-md-2 hidden-xs">
                    <div class="bg-main">
                        

                    </div>
                </div>
                <div id="MainContentContainer" class="col-md-8">
                    <div class="bg-main">
 
 
<form name="form1" action="LoginServlet" method="post">

<div class = "login">
	<div class = "login-screen"> 
		<div class = "app-title">
			<h1>Sign In </h1>
		</div>
	
		<div class = "login-form">
			<div class = "control-group">
				<input type="text" class  = "login-field" placeholder="username" name="userid">
				<label class="login-field-icon fui-user" for="login-name"></label>
			</div>
			<div class  = "control-group">
				<input type="password" class="login-field" placeholder="password" name="password">
				<label class="login-field-icon fui-lock" for="login-pass"></label>
			</div>
			<input class="btn btn-primary btn-large btn-block" type="submit" value="Login"></input>
		</div>
	</div>
</div>
<!--<table align="center">
<tr>
<td>Username</td>
<td><input type="text" name="userid" /></td>
</tr>
<tr>
<td>Password</td>
<td><input type="password" name="password" /></td>
</tr>
<tr>
<tr>
	<button><a href="Register.jsp" id="registerId">Register</a></button>
</tr>
<td><%=(request.getAttribute("errMessage") == null) ? "" : request.getAttribute("errMessage")%></td>
</tr>
<tr>
<td></td>
<td><input type="submit" value="Login"></input>
<input type="reset" value="Reset"></input>

</td>
</tr>
</table>-->
 
</form>
</body>
</html>