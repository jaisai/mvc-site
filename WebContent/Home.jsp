<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
 <head>
 	<title>Home Page</title>
 	 <link rel="shortcut icon" href="favicon.ico" />
<link rel="stylesheet" href="css/Login.css" type="text/css" />
<link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
<link rel="stylesheet" href="css/style.css" type="text/css" />
<link rel="stylesheet" href="css/Home.css" type="text/css">
 <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
 </head>
 <body>

<ul>
  <li><a class="active" href="Home.jsp"><img alt="Home" src="img/logo.png" ></img></a></li>
  <li><a href="Details.jsp" style="text-decoration: none">Details</a></li>
  <li><a href="Assesments.jsp" style="text-decoration: none">Assesments</a></li>
  <li><a href="Admin.jsp" style="text-decoration: none">Admin</a></li>
</ul>

<table class="table table-hover" cellspacing="0"
							style="border-collapse: collapse;">
							<tr>
								<th class="bg-primary" scope="col" style="border-width: 0px;">Assessment
									Name</th>
								<th class="bg-primary" scope="col" style="border-width: 0px;">Date</th>
								<th class="bg-primary" scope="col" style="border-width: 0px;">Location</th>
								<th class="bg-primary" scope="col" style="border-width: 0px;">&nbsp;</th>
							</tr>
							<%
								Iterator itr;
								List data = (List) request.getAttribute("assessList");
								for (itr = data.iterator(); 
										itr.hasNext();) {
							%>
							<tr>
								<td><%=itr.next()%></td>
								<td><%=itr.next()%></td>
								<td><%=itr.next()%></td>
								<td><input type="submit"
									class="btn btn-primary pull-right" value="Details"
									style="margin-left: 5px;"> <input type="submit"
									class="btn btn-success pull-right" value="Register"></td>
							</tr>

							<%
								}
							%>

</table>


 </body>
 </html>