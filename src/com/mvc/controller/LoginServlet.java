package com.mvc.controller;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.sql.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.mvc.bean.LoginBean;
import com.mvc.dao.HomeDao;
import com.mvc.dao.LoginDao;
import com.mvc.util.DBConnection;
 
public class LoginServlet extends HttpServlet {

private String sql = "select * from assessments.assessments";
ResultSet rs;

public LoginServlet() {
	
 }

List<Object> dlist = new ArrayList<>();

 
protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
 
//Here username and password are the names which I have given in the input box in Login.jsp page. Here I am retrieving the values entered by the user and keeping in instance variables for further use.
 
String userid = request.getParameter("userid");
String password = request.getParameter("password");
 
LoginBean loginBean = new LoginBean(); //creating object for LoginBean class, which is a normal java class, contains just setters and getters. Bean classes are efficiently used in java to access user information wherever required in the application.
 
loginBean.setUserName(userid); //setting the username and password through the loginBean object then only you can get it in future.
 loginBean.setPassword(password);
 
LoginDao loginDao = new LoginDao(); //creating object for LoginDao. This class contains main logic of the application.
 
String userValidate = loginDao.authenticateUser(loginBean); //Calling authenticateUser function
 
if(userValidate.equals("SUCCESS")) //If function returns success string then user will be rooted to Home page
 {
 request.setAttribute("userid", userid); //with setAttribute() you can define a "key" and value pair so that you can get it in future using getAttribute("key")

 
 /*	try {
 		Statement S = DBConnection.createConnection().createStatement();
 		S.executeQuery(sql);
 		rs = S.getResultSet();
 		dlist.clear();
 		while(rs.next())
 		{
 			dlist.add(rs.getString("adminUserID"));
 			dlist.add(rs.getString("adminFirstName"));
 			dlist.add(rs.getString("adminLastName"));
 			//dlist.add(rs.getString("email"));
 		}
 		
 		rs.close();
 		S.close();
 		
 		} 	
 	catch (SQLException e) 
 	{
 		e.printStackTrace();
 	} */
 
 	List<Object> list = new ArrayList<>();
 	list = HomeDao.getAssessList();
 	System.out.println("jai list in servlet:" + list);
 	request.setAttribute("assessList", list);
 	RequestDispatcher disp = request.getRequestDispatcher("/Home.jsp");
 	if(disp != null)
 	{
 		disp.forward(request, response);
 	}
 	 request.getRequestDispatcher("/Home.jsp").forward(request, response);//RequestDispatcher is used to send the control to the invoked page.
 }

 else
 {
 request.setAttribute("errMessage", userValidate); //If authenticateUser() function returnsother than SUCCESS string it will be sent to Login page again. Here the error message returned from function has been stored in a errMessage key.
 request.getRequestDispatcher("/Login.jsp").forward(request, response);//forwarding the request
 }
 }

 
}