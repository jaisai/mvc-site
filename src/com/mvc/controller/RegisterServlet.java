package com.mvc.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mvc.dao.LoginDao;
import com.mvc.util.DBConnection;

/**
 * Servlet implementation class RegisterServlet
 */

public class RegisterServlet extends HttpServlet {
//	private static final long serialVersionUID = 1L;
//       
//    /**
//     * @see HttpServlet#HttpServlet()
//     */
//    public RegisterServlet() {
//        super();
//        // TODO Auto-generated constructor stub
//    }
//
//	/**
//	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
//	 */
//	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		// TODO Auto-generated method stub
//		response.getWriter().append("Served at: ").append(request.getContextPath());
//	}
//
//	/**
//	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
//	 */
	
public RegisterServlet()
{
	
}
protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//doGet(request, response);
	
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		String n = request.getParameter("userid");
		String p = request.getParameter("password");
		String fn = request.getParameter("fname");
		String ln = request.getParameter("lname");
		String e = request.getParameter("email");
		
		try
		{
			DBConnection con = new DBConnection();
			PreparedStatement ps = DBConnection.createConnection().prepareStatement("insert into assessments.test values(?,?,?,?,?)");
			ps.setString(1, n);
			ps.setString(2, p);
			ps.setString(3, fn);
			ps.setString(4, ln);
			ps.setString(5, e);
			
			int i = ps.executeUpdate();
			if(i>0)
			{
				out.print("Successfully Registered");
				
			}
		}catch (Exception e2) {
			// TODO: handle exception
			System.out.println(e2);
		}
		out.close();
	}

}
