package com.mvc.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import com.mvc.bean.LoginBean;
import com.mvc.util.DBConnection;

public class LoginDao {
	public String authenticateUser(LoginBean loginBean) {

		String userid = loginBean.getUserName(); // Keeping user entered
													// values in temporary
													// variables.
		String password = loginBean.getPassword();

		// Connection con = null;
		// Statement statement = null;
		// RessultSeultSet ret = null;

		String userNameDB = "";
		String passwordDB = "";

		try {
			Connection con = DBConnection.createConnection(); // establishing
																// connection
			Statement statement = con.createStatement(); // Statement is used to
															// write queries.
															// Read more about
															// it.
			ResultSet resultSet = statement.executeQuery("select userid,password from test");

			while (resultSet.next()) // Until next row is present otherwise it
										// return false
			{
				userNameDB = resultSet.getString("userid"); // fetch the
																// values
																// present in
																// database
				passwordDB = resultSet.getString("password");

				if (userid.equals(userNameDB) && password.equals(passwordDB)) {
					return "SUCCESS"; //// If the user entered values are
										//// already present in database, which
										//// means user has already registered
										//// so I will return SUCCESS message.
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return "Invalid user credentials"; // Just returning appropriate message
											// otherwise
	}

}
