package com.mvc.bean;
 
//As I have already told it contains only setters and getters
 
public class LoginBean
 {
 private String userid;
 private String password;
 
public String getUserName() {
 return userid;
 }
public void setUserName(String userid) {
 this.userid = userid;
 }
 public String getPassword() {
 return password;
 }
 public void setPassword(String password) {
 this.password = password;
 }
 }